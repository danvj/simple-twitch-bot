COLOURS = {
    "GREEN": "\033[92m",
    "YELLOW": "\033[93m",
    "RED": "\033[91m",
    "RESET": "\033[0m"
}

TIMEFORMAT = "%Y-%m-%d %H:%M:%S"
