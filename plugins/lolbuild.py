import random


def lolbuild(params):

    boots = ["Mercury", "Berserker"]

    finalitems = [
        "Phantom Dancer",
        "Bloodthirster",
        "Infinity Edge",
        "BORK",
        "Static Shiv",
    ]

    build = [random.choice(finalitems) for i in range(5)]
    build.append(random.choice(boots))

    return "@user you will have to build: " + ", ".join(build)
