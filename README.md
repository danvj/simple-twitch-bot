# Simple Twitch Bot

Simple and highly extensible twitch bot coded in pure Python.

## Quick-start

- Make sure you have **Python>=3.9** installed on your computer.
- Clone or download this repository.
- Make a copy of `config_default.json` and rename it to `config.json`.
- Edit the `config.js` file (see below).
- Run the bot with `python bot.py`.

I also advise creating a spare twitch account to be used as your bot.

## config.json

The general config layout should look something like this:

```json
{
  "prefix": "!",
  "BOT_USERNAME": "username",
  "OAUTH_TOKEN": "oauth:xxxxxxxxxxxxxxxxxxxxxxxxxx",
  "CHANNEL": "#twitchchannel",
  "welcome": "Hello chat!",
  "cycle_timer": 300,
  "cycle": ["LUL", "Meow", "Bzz bzz I'm a bot!"],
  "commands": {
    "help": "No help for you @user!",
    "youtube": "https://youtube.com/professoratum/",
    "uptime": "Uptime: @uptime",
    "site": "https://danvj.pt/"
  },
  "plugins": ["flip"]
}
```

You can edit this file to your liking, but the main things you need to change are:

- `BOT_USERNAME` put the username of your spare twitch account
- `OAUTH_TOKEN` you can get this by going to <https://twitchapps.com/tmi>
- `CHANNEL` put the name of the channel you want your bot to connect to (e.g. "#professoratum")

The rest of the settings are optional, but you should change them:

- `welcome` defines the messages the bot says when it joins the chat.
- `cycle_timer` defines, in seconds, the time intervals in which the bot will post the messages included in the `cycle` variable
- `prefix` defines the prefix for your bot commands. In most cases you will use "!" in commands such as "!help"
- `cycle` you can add pieces of text that your bot will say every x amount of seconds. The time interval is defined by the `cycle_time` variable
- `commands` you can define commands here that you want your bot to reply to and define its answer (e.g. if you define in the config file `"hello": "Hi there!"` when someone types `!hello` in the chat the bot will reply with `Hi there!`). You can also use special placeholders that you can check below.
- `plugins` here you can choose which plugins you wish to be active or inactive by setting the variable value to `true` or `false` respectively. More information about the plugins can be found in the Plugins section.

## Commands

Currently, there are 2 placeholders that you can use in messages for the bot to use:

- `@user` will mention the user that typed the command.
  - e.g. if you have `"hello": "Hi there @user!"`, someone typing `!hello` in chat would result in a reply of `"Hi there professoratum!"` (assuming that your username is professoratum.
- `@uptime` you can use this to print the uptime of your bot. If you want it to match the stream's uptime, start the bot and the stream at the same time. The format of the @uptime is `x hours, y minutes and z seconds`.

## Plugins

There are currently 2 fully working plugins bundled with the bot: `flip` and `lolroll`. To activate a plugin you should add it to the `config.json` file.

Example:

```json
...
    "plugins": ["flip"]
    }
```

### Bundled plugins

- `flip` Type !flip in chat to get Heads or Tails
- `lolroll` Type !lolroll in chat to get a random LoL champion.

### Creating Plugins

You can create plugins very easily. As example, let's say we want to create the !dice plugin/command.

First, create a `dice.py` file inside the plugins' folder. Inside this file you should have a function named "dice" with a "params" argument and having it return a string.

Example:

```python
def dice(params):
    return "I am a dice plugin"

```

Now you should have the logic of the command inside the `dice` function.

```python
from random import randint

def dice(params):
    return f"Hey @user, you rolled a {randint(1,6)}!"

```

**Notes:**

- The name of the plugin file determines the command alias.
- Your plugin file **MUST HAVE** a function with the same name.
- You can use the placeholders from the commands section in the string returned by the plugin.
- The `params` argument that is unused in this example assumes the value of everything that was typed after the command. Example: if someone typed `!dice 12` in chat, the `params` would be `12`. This can be useful for creating more interactive plugins.
