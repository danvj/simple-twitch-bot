from connection import Connection
import json
from datetime import datetime
import math
from constants import COLOURS
from importlib import import_module


class Bot:
    def __init__(self, name: str = "Cookie"):
        self.name = name
        self._author: str = "DanVJ (https://www.danvj.pt/)"
        self._config = None
        self._connection = None
        self.start_time: datetime = datetime.now()
        self.timer: datetime = datetime.now()
        self.cycle_index: int = 0
        self.loaded_plugins: list[str] = list()

    @property
    def conn(self):
        if self._connection is None:
            self._connection = Connection(
                self.config["BOT_USERNAME"],
                self.config["OAUTH_TOKEN"],
                self.config["CHANNEL"],
            )
        return self._connection

    @property
    def config(self):
        if self._config is None:
            self._config = json.load(open("./config.json"))
        return self._config

    def say_author(self):
        self.conn.send_message(f"My author is {self._author}!")

    def check_timer(self):
        time_diff = datetime.now() - self.timer
        time_reset = self.config.get("cycle_timer", 300)
        if time_diff.seconds / time_reset > 1:
            self.timer = datetime.now()
            self.cycle_messages()

    def cycle_messages(self):
        total_messages: int = len(self.config.get("cycle", []))
        if not total_messages:
            return
        if self.cycle_index == total_messages:
            self.cycle_index = 0
        self.conn.send_message(self.config["cycle"][self.cycle_index])
        self.cycle_index += 1

    def read_message(self, new_message: tuple[str, str]) -> None:

        if new_message == ("", ""):
            return

        prefix = new_message[1][0]

        if prefix != self.config.get("prefix", "!"):
            return

        user = new_message[0]

        interaction = new_message[1][1:].split(" ")

        command = interaction[0]
        if len(interaction) > 1:
            self.reply(user, command, interaction[1])
        else:
            self.reply(user, command)

    def reply(self, user: str, command: str, params: str = ""):

        if command in self.config["commands"]:
            reply_message = self.sanitize_message(
                user, self.config["commands"][command]
            )

        elif command in self.loaded_plugins:
            plugin = import_module(f"plugins.{command}")
            run_plugin = getattr(plugin, command)
            reply_message = self.sanitize_message(user, run_plugin(params))

        else:
            self.conn.log_message(
                f'Unkown command "{command}" requested by "{user}".', COLOURS["RED"]
            )
            return

        self.conn.send_message(reply_message)

    def sanitize_message(self, user: str, message: str) -> str:
        if "@user" in message:
            message = message.replace("@user", user)
        if "@uptime" in message:
            time_now = datetime.now() - self.start_time
            hrs = math.floor(time_now.seconds / 3600)
            mins = math.floor(time_now.seconds / 60)
            secs = time_now.seconds - (mins * 60) - (hrs * 3600)
            uptime = f"{hrs} hours, {mins} minutes and {secs} seconds"
            message = message.replace("@uptime", uptime)

        return message

    def load_plugins(self):
        if not len(self.config.get("plugins", [])):
            return
        for plugin in self.config["plugins"]:
            self.loaded_plugins.append(plugin)
            import_module(f"plugins.{plugin}")  # Add custom error
        loaded = f"{' '.join(str(plugin) for plugin in self.loaded_plugins)}"
        self.conn.log_message(f"Plugins loaded: {loaded}", COLOURS["GREEN"])


def main():

    bot = Bot()

    bot.load_plugins()
    bot.conn.connect()
    # Message to be sent when connected
    bot.conn.send_message(bot.config.get("welcome", "Hello chat!"))

    while True:  # Infinite loop
        bot.conn.write_buffer()
        bot.read_message(bot.conn.read_buffer())
        bot.check_timer()


if __name__ == "__main__":
    main()
