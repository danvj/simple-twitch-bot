import socket
import time
import select
from constants import TIMEFORMAT, COLOURS


class Connection:
    """
    Connection class
    """

    def __init__(
        self,
        USERNAME: str,
        TOKEN: str,
        CHANNEL: str,
        HOST: str = "irc.twitch.tv",
        PORT: int = 6667,
    ) -> None:

        self.sock = socket.socket()

        self.USERNAME: str = USERNAME
        self.CHANNEL: str = CHANNEL

        self.sock.connect((HOST, PORT))

        self.sock.send(f"PASS {TOKEN}\r\n".encode())
        self.sock.send(f"NICK {USERNAME}\r\n".encode())
        self.sock.send(f"JOIN {CHANNEL}\r\n".encode())

        self.message_template = f"PRIVMSG {CHANNEL} :"
        self.message_buffer: list[str] = list()

        self.connected: bool = False

    def connect(self) -> None:
        """Connects to the IRC server.

        :raises TimeoutError: The connection took more than
        20 seconds to be established.
        """
        connecting: bool = True
        timeout: int = 20

        self.log_message(
            f"Connecting to {self.CHANNEL} as {self.USERNAME}",
            COLOURS["YELLOW"],
        )

        while connecting:
            if timeout < 1:
                raise TimeoutError

            self.write_buffer()
            self.read_buffer()
            timeout -= 1
            time.sleep(1)

            if len(self.message_buffer) != 0:
                if "End of /NAMES list" in self.message_buffer[0]:
                    connecting = False

        self.connected = True
        self.log_message("Connected to chat!")

    def write_buffer(self):
        """If the message buffer is empty,
        retrieves more messages from the
        server.
        """
        if len(self.message_buffer) != 0:
            return
        ready = select.select([self.sock], [], [], 1)
        if not ready[0]:
            return
        new_feed = self.sock.recv(1024)
        new_messages: list[str] = new_feed.decode().split("\r\n")
        for message in new_messages:
            if message == "":
                continue
            self.message_buffer.append(message)
        else:
            self.message_buffer.append("")

    @property
    def time_now(self) -> str:
        """Returns the current time formated.

        :return: Current time formated.
        :rtype: str
        """

        return time.strftime(TIMEFORMAT, time.localtime())

    def log_message(
        self, message: str, colour: str = COLOURS["RESET"]
    ) -> None:
        """Logs messages to the console.

        :param message: Message to be displayed to the console.
        :type message: str
        :param color: Color for the message.
        :type color: str
        """

        print(f"[{self.time_now}] {colour}{message}{COLOURS['RESET']}")

    def send_message(self, message: str) -> None:
        """Sends a message to chat.

        :param message: Message to be sent.
        :type message: str
        """

        self.sock.send(f"{self.message_template}{message}\r\n".encode())
        self.log_message(
            f"{COLOURS['GREEN']}Sent message:{COLOURS['RESET']} " f"{message}"
        )

    def ping_pong(self, ping: str):
        """Replies to the ping message with a pong message. This prevents
        a connection timeout to the server.

        :param ping: Ping message received.
        :type ping: str
        """

        self.log_message(f"!!! Ping received: {ping}", COLOURS["RED"])
        pong: str = ping.replace("PING", "PONG")
        self.sock.send(f"{pong}\r\n".encode())
        self.log_message(f"!!!Pong sent: {pong}", COLOURS["GREEN"])

    def read_buffer(self) -> tuple[str, str]:

        if len(self.message_buffer) == 0:
            self.message_buffer.append("")
            return ("", "")

        first_message = self.message_buffer[0]
        self.message_buffer.pop(0)

        if "PING :tmi.twitch.tv" in first_message:
            self.ping_pong(first_message)
            return ("", "")

        parsed_message = first_message.split(":")
        # print(parsed_message)
        if len(parsed_message) < 3:
            return ("", "")
        content = parsed_message[-1]
        user = parsed_message[1].split("!")[0]
        self.log_message(
            f"New message from {COLOURS['GREEN']}{user}:"
            f"{COLOURS['RESET']} {content}",
            COLOURS["YELLOW"],
        )
        return (user, content)
